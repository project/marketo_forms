# Marketo JS Forms Integration

This module allows you to create Marketo form blocks on your Drupal sites.

This module comes with a new field type so
each entity can have it's own unique form.

The provided CKEditor plugin allows editors to easily embed
a Marketo Form into content by adding the token:
```
[marketo-form:FORM_ID]
```

For a full description of the module, visit the
[project page](https://www.drupal.org/project/marketo_forms).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/marketo_forms).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Contributing
- Maintainers


## Requirements

- Core block module
- Core field module


## Installation

- Install as you would normally install a contributed Drupal module. For further
  information, see
  [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
- Enable module
- Enter Marketo API key at Administration >> Configuration >> Web Services >> Marketo Forms


## Configuration

1. Visit Administration >> Structure >> Block
2. Click `'Place block'` give your block a name
3. Select the appropriate form from the provided dropdown.


## Troubleshooting

- Clear Drupal cache after adding or changing a Marketo key


## Contributing

- Create an issue and attach a patch.
- Master branch is default for new contributions.


## Maintainers

- Ivan Abramenko - [levmyshkin](https://www.drupal.org/u/levmyshkin)
- Loganathane Virassamy - [vlogus](https://www.drupal.org/u/vlogus)
