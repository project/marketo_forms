(function ($, window, Drupal, MktoForms2) {
  Drupal.behaviors.marketoForms = {
    attach: function attach(context, settings) {
      if (!MktoForms2) {
        return;
      }

      var marketoConfig = settings.marketoForms;
      if (!marketoConfig) {
        return;
      }

      var host = marketoConfig.host;
      var munchkinId = marketoConfig.apiKey;
      if (!host || !munchkinId) {
        return;
      }

      $(once('marketo-forms', 'form[id^=mktoForm_]', context)).each(function (index) {
        var $form = $(this);
        var htmlId = $form.attr('id');
        var formId = htmlId.replace(/^mktoForm_/, '');
        MktoForms2.loadForm(host, munchkinId, formId);
      });
    }
  };
})(jQuery, window, Drupal, MktoForms2);
