<?php

namespace Drupal\marketo_forms\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure site information settings for this site.
 */
class MarketoSettingsForm extends ConfigFormBase {

  /**
   * Summary of cacheTagsInvalidator.
   *
   * @var mixed
   */
  protected $cacheTagsInvalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->cacheTagsInvalidator = $container->get('cache_tags.invalidator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marketo_forms_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['marketo_forms.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('marketo_forms.settings');
    $host = $config->get('marketo_host_key');
    $api_key = $config->get('marketo_api_key');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => !empty($host) && !empty($api_key),
    ];

    $state_conditions = [
      ':input[name="enabled"]' => [
        'checked' => TRUE,
      ],
    ];
    $states = [
      'visible' => $state_conditions,
      'required' => $state_conditions,
    ];

    $form['marketo_host_key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Marketo Hostname'),
      '#default_value' => $host,
      '#description'   => $this->t('The hostname for referencing Marketo code used on the site (Usually in format app-XXXX.marketo.com).'),
      '#states' => $states,
    ];

    $form['marketo_api_key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Marketo API Key'),
      '#default_value' => $api_key,
      '#description'   => $this->t('The Marketo Munchkin ID for the site (usually in format XXX-XXX-XXX).'),
      '#states' => $states,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $input_names = [
      'marketo_host_key',
      'marketo_api_key',
    ];

    $is_enabled = $form_state->getValue('enabled');
    if ($is_enabled) {
      foreach ($input_names as $name) {
        $value = $form_state->getValue($name);
        $element_title = $form[$name]['#title'] ?? '';
        if (!isset($value) || trim($value) === '') {
          $form_state->setError(
            $form[$name],
            $this->t('@name field is required.', ['@name' => $element_title])
          );
        }
      }
    }
    else {
      // Erase values in case the forms are disabled.
      foreach ($input_names as $name) {
        $form_state->setValue($name, '');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('marketo_forms.settings');

    // Enable/Disable plugin.
    $config->set('marketo_host_key', $form_state->getValue('marketo_host_key'))
      ->set('marketo_api_key', $form_state->getValue('marketo_api_key'))
      ->save();

    // Rebuild the forms library, as it depends on settings updated here.
    $this->cacheTagsInvalidator->invalidateTags(['library_info']);

    parent::submitForm($form, $form_state);
  }

}
