<?php

namespace Drupal\marketo_forms\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "MarketoForms" plugin.
 *
 * @CKEditorPlugin(
 *   id = "marketo_forms",
 *   label = @Translation("Marketo Forms")
 * )
 */
class MarketoForms extends PluginBase implements CKEditorPluginInterface, CKEditorPluginButtonsInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->configFactory = $container->get('config.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::service('extension.list.module')->getPath('marketo_forms') . '/assets/marketo_forms.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'marketo_forms' => [
        'label' => t('Marketo Forms'),
        'image' => \Drupal::service('extension.list.module')->getPath('marketo_forms') . '/assets/icon.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = $this->configFactory->get('marketo_forms.settings');
    $host = $config->get('marketo_host_key');
    $api_key = $config->get('marketo_api_key');

    return [
      'marketo_forms_enabled' => !empty($host) && !empty($api_key),
      'marketo_forms_dialog_title' => t('Marketo Forms'),
    ];
  }

}
