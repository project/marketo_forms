<?php

namespace Drupal\marketo_forms\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Render Marketo Forms.
 *
 * @Filter(
 *   id = "marketo_forms",
 *   title = @Translation("Marketo Forms"),
 *   description = @Translation("Substitutes [marketo-forms:FORM_ID] with embedded marketo forms."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class MarketoForms extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->configFactory = $container->get('config.factory');
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (preg_match_all('/\[marketo\-form(\:(.+))?( .+)?\]/isU', $text, $matches_code)) {
      $config = $this->configFactory->get('marketo_forms.settings');
      $result->addCacheableDependency($config);

      $host = $config->get('marketo_host_key');
      $api_key = $config->get('marketo_api_key');

      foreach ($matches_code[0] as $ci => $code) {
        $form = [
          'form_id' => $matches_code[2][$ci],
        ];
        // Override default attributes.
        if (!empty($matches_code[3][$ci]) && preg_match_all('/\s+([a-zA-Z_]+)\:(\s+)?([0-9a-zA-Z\/]+)/i', $matches_code[3][$ci], $matches_attributes)) {
          foreach ($matches_attributes[0] as $ai => $attribute) {
            $form[$matches_attributes[1][$ai]] = $matches_attributes[3][$ai];
          }
        }
        $element = [
          '#theme' => 'marketo_form',
          '#host' => $host,
          '#api_key' => $api_key,
          '#form_id' => $form['form_id'],
        ];

        $replacement = $this->renderer->executeInRenderContext(new RenderContext(), function () use (&$element) {
          return $this->renderer->render($element);
        });
        $result = $result->merge(BubbleableMetadata::createFromRenderArray($element));
        $text = str_replace($code, $replacement, $text);
      }

      $result->setProcessedText($text);
    }

    return $result;
  }

}
